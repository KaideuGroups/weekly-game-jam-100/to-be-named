extends Camera2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
	var UP = Input.is_action_pressed("ui_up")
	var DOWN = Input.is_action_pressed("ui_down")
	var LEFT = Input.is_action_pressed("ui_left")
	var RIGHT = Input.is_action_pressed("ui_right")
	
	if UP:
		position.y -= 10*zoom.x
	if DOWN:
		position.y += 10*zoom.x
	if LEFT:
		position.x -= 10*zoom.x
	if RIGHT:
		position.x += 10*zoom.x


func _input(event):
	var SCROLL_OUT = Input.is_action_just_pressed("Scroll_Out")
	var SCROLL_IN = Input.is_action_just_pressed("Scroll_In")
	
	
	zoom.x = clamp(zoom.x, 0.2, 1)
	zoom.y = clamp(zoom.y, 0.2, 1)
	
	if SCROLL_IN :
		zoom -= Vector2(0.05,0.05)
	if SCROLL_OUT:
		zoom += Vector2(0.05,0.05)