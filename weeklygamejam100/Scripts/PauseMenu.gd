extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	hide()

func _input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		Global.toggle_pause_game("switch")
		visible = !visible


func _on_Resume_pressed():
	Global.toggle_pause_game(false)
	hide()


func _on_Quit_pressed():
	get_tree().quit()
