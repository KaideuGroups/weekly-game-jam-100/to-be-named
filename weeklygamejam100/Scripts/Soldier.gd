extends KinematicBody2D

var click_position = Vector2()
var unit_moving = false
export var unit_speed = 0
export var over_shoot_amount = 1.2
var group = null
var get_pos = null
var parent = null
var attacking = false
var attacked = false
var health = 100
var ray_to


var target = null

func _ready():
	parent = get_parent()
	unit_speed = parent.SPEED

func _physics_process(delta):
	if unit_moving == true:
		calc_move(position, click_position, unit_speed, delta)
		if !parent.moving:
			parent.moving = true
	update()
	
func _draw():
	if $shot_timer.is_stopped() == false:
		draw_line(Vector2(), ray_to, Color(0.7,0.4,0.4,0.5), 2.0,false)
	
	
	if target != null and attacking == false:
		start_attacking()
		attacking = true
	if target == null:
		cease_fire()
		attacking = false


func _input(event):
	if Input.is_action_just_released("LEFT_CLICK") and parent.selected and !parent.able:
		unit_speed = parent.SPEED + rand_range(-3,3)
		click_position = get_global_mouse_position() - get_pos
		unit_moving = true


func calc_move(from, to, speed, delta):
	var dir = (to - from).normalized()
	move_and_slide(dir * speed)
	if from.x - to.x <= 5 and from.x - to.x >= -5:
		if from.y - to.y <= 5 and from.y - to.y >= -5:
			unit_moving = false
	animate(dir)



# calculates direction might have to be changed to platoon for performance
func animate(direction):
	direction = rad2deg(direction.angle())
	if direction > 135 or direction < -135:
		$anim.play("LEFT")
	elif direction > -45 and direction < 45:
		$anim.play("RIGHT")
	elif direction > 45 and direction < 135:
		$anim.play("DOWN")
	else:
		$anim.play("UP")


func fire():
	modulate.a = .5
	yield(get_tree().create_timer(.1, true), "timeout")
	modulate.a = 1
	
	if check_if_freed(target) == false:
		ray_to = calc_attack_cone(target.global_position - global_position)
		$ray2target.set_cast_to(ray_to)
		$shot_timer.start()
	#damage collider 	


func _on_shot_timer_timeout():
	var shot = $ray2target.get_collider()
	if shot != null:
		if shot.name.substr(0,5) == "enemy":
			shot.take_hit(rand_range(get_parent().DAMAGE[0], get_parent().DAMAGE[1]))



func calc_attack_cone(vector):
	var length = vector.length()
	vector = vector.angle()
	vector += rand_range(-get_parent().ACCURACY,get_parent().ACCURACY)
	var new_vector = (Vector2(cos(vector),sin(vector)) * length * over_shoot_amount)
	return new_vector

func take_hit(damage):
	modulate.r = 5
	yield(get_tree().create_timer(.1, true), "timeout")
	modulate.r = 1
	health -= damage
	if health <= 0:
		queue_free()


func start_attacking():
	yield(get_tree().create_timer(float(rand_range(.5, 1)),true),"timeout")
	_on_shot_timer_timeout()
	$Timer.set_wait_time(float(rand_range(parent.FIRE_RATE[0], parent.FIRE_RATE[1])))
	$Timer.start()
	


func cease_fire():
	target = null
	$Timer.stop()


func _on_Area2D_mouse_entered():
	if !parent.able:
		parent.able = true

func _on_Area2D_mouse_exited():
	if parent.able:
		parent.able = false


func _on_Timer_timeout():
	fire()
	$Timer.set_wait_time(float(rand_range(parent.FIRE_RATE[0], parent.FIRE_RATE[1])))




func check_if_freed(node):
	if node != null:
		var wr = weakref(node)
		if (!wr.get_ref()):
			return  true
		else:
			return  false
	else:
		return true


