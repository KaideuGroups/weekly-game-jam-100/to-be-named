extends Node

# Global Singleton - Game Manager

var GameOver = false

var current_scene = null
var get_main_menu = preload("res://Scenes/MainMenu.tscn")
var get_hud = preload("res://Scenes/HUD.tscn")
var get_pause_menu = preload("res://Scenes/PauseMenu.tscn")
var get_game_over = preload("res://Scenes/GameOver.tscn")

var over_ui = false


func _ready():
	randomize()
	grab_current_scene()


#Get current scene in tree and update the variable
func grab_current_scene():
	current_scene = get_tree().get_current_scene()


#Loads the next game scene given, then loads UI if needed
func load_scene(scene):
	get_tree().change_scene("res://Scenes/" + str(scene) + ".tscn")


#Loads UI scenes
func load_ui():
	var HUD = get_hud.instance()
	var PAUSE = get_pause_menu.instance()

	current_scene.get_node("Canvas").add_child(HUD)
	current_scene.get_node("Canvas").add_child(PAUSE)


#Ends game by setting GameOver and displaying GameOver overlay scene
func game_over():
	GameOver = true
	var GAMEOVER = get_game_over.instance()
	current_scene.get_node("Canvas").add_child(GAMEOVER)


#goes back to main menu and resets gameover
func reset_game():
	load_scene("MainMenu")
	GameOver = false
	toggle_pause_game(false)


#toggle game process state
func toggle_pause_game(state):
	match state:
		true:
			get_tree().paused = state
		false:
			get_tree().paused = state
		"switch":
			get_tree().paused = !get_tree().paused

