extends KinematicBody2D


onready var purple_texture = preload("res://Assets/enemys/type2.png")

export var over_shot_amount = 1.2
export var RANGE = 200
export var SPEED = 35
export var HEALTH = 100
export var TYPE = 1
export var accuracy = 0.3
export var DAMAGE = [5,10]
var enemy_moving = false
onready var range_node = get_node("Area2D")
var target = null
var target_looking = false
var group = "enemy"
onready var current_position = get_global_position()
var new_vector


func _ready():
	$Control/health_bar.max_value = HEALTH
	$Control/health_bar.visible = false
	if TYPE == 2:
		$Sprite.texture = purple_texture



func _process(delta):

	if target != null and enemy_moving == true:
		$attack_cooldown.start()
		enemy_moving = false
	elif target == null:
#move to drill if no target
		$attack_cooldown.stop()
		enemy_moving = true
		calc_move(position, get_parent().get_node("Drill").get_global_position(), SPEED)
	update()
	
func _draw():
	if $shot_timer.is_stopped() == false:
		draw_line(Vector2(), new_vector, Color(0.4,0.4,0.8,0.5), 2.0,false)

func fire():
	modulate.a = .5
	yield(get_tree().create_timer(.1, true), "timeout")
	modulate.a = 1
	
	if check_if_freed(target) == false:
		var ray_to = calc_attack_cone(target.global_position - global_position, over_shot_amount)
		$ray2target.set_cast_to(ray_to)
		$shot_timer.start()
	#damage collider


func calc_attack_cone(vector, extend):
	var length = vector.length()
	vector = vector.angle()
	vector += rand_range(-accuracy,accuracy)
	new_vector = (Vector2(cos(vector),sin(vector)) * length * extend)
	return new_vector

func _on_shot_timer_timeout():
	var shot = $ray2target.get_collider()
	if shot != null:
		shot.take_hit(rand_range(DAMAGE[0], DAMAGE[1]))



func _on_attack_cooldown_timeout():
	if target != null:
		fire()

func calc_move(from, to, speed):
	var dir = (to - from).normalized()
	move_and_slide(dir * speed)
	if from.x - to.x <= 10 and from.x - to.x >= -10 and from.y - to.y <= 10 and from.y - to.y >= -10:
			enemy_moving = false
	#animate(dir)


#not used yet
func animate(direction):
	direction = rad2deg(direction.angle())
	if direction > 135 or direction < -135:
		$anim.play("LEFT")
	elif direction > -45 and direction < 45:
		$anim.play("RIGHT")
	elif direction > 45 and direction < 135:
		$anim.play("DOWN")
	else:
		$anim.play("UP")





func take_hit(damage):
	$Control/health_bar.visible = true
	modulate.r = 5
	yield(get_tree().create_timer(.1, true), "timeout")
	modulate.r = 1
	HEALTH -= damage
	if HEALTH <= 0:
		if check_if_freed(self) == false:
			Global.current_scene.enemies_left -= 1
		Global.current_scene.check_enemy_count()
		queue_free()
	$Control/health_bar.value = HEALTH


func _on_Area2D_area_entered(area):
	if area.name == "enemy_detection" and target == null:
		target = area
		enemy_moving = true


#search 
func search_overlapping():
	for k in $Area2D.get_overlapping_areas():
		if k.name == "enemy_detection":
			target = k
			enemy_moving = true



#check distance every second if larger then disengage range then search overlapping areas for new target
func _on_new_target_timeout():
	if get_distance_to_target() == 0:
		target = null
		search_overlapping()
	if get_distance_to_target() > RANGE + 30:
		target = null
		search_overlapping()


func get_distance_to_target():
	if target == null:
		return 0
	elif target != null:
		var wr = weakref(target)
		if (!wr.get_ref()):
			return 0
		else:
			return (position - target.get_global_position()).length()


func check_if_freed(node):
	if node != null:
		var wr = weakref(node)
		if (!wr.get_ref()):
			return  true
		else:
			return  false

