extends Node2D

var get_platoon = preload("res://Scenes/units/Platoon.tscn")
var enemy_ref = preload("res://Scenes/enemys/enemy_reg.tscn")
var off_wave1 = preload("res://Assets/music/Off-Wave_1_-_The_Worst_is_Yet_to_Come_2.ogg")
var off_wave2 = preload("res://Assets/music/Off-Wave_2_-_Prepare_Yourselves.ogg")
var off_wave4 = preload("res://Assets/music/Off-Wave_4_-_The_Here_and_Now.ogg")
var combat1 = preload("res://Assets/music/Combat_1_-_Hold_Your_Ground.ogg")
var combat2 = preload("res://Assets/music/Combat_2.ogg")
var maintheme = preload("res://Assets/music/Main_Theme_Final_Draft.ogg")


onready var music = $music_player


#going to get platoon healths
var pHEALTH = [0, 0, 0, 0, 0, 0]

var selected = []
var wave = 0
var salvage = 0
var enemies_left  = 0
var active_wave = false
onready var wave_label = get_node("Canvas/HUD/Wave")
var enemy_spawns = {
	left_side = [Vector2(-400,570),Vector2(-400,-900)],
	top_side = [Vector2(-300,-1060),Vector2(1700,-1060)],
	right_side = [Vector2(1780,570),Vector2(1780,-900)]
	}

var UNITS = []
var alive_enemies = []

func _ready():
	music.play()
	Global.grab_current_scene()
	spawn_units(6)
	Global.load_ui()
	for k in get_children():
		print(k.name)
	

func _process(delta):
	assign_labels()

func _input(event):
	var LEFT_CLICK = Input.is_action_just_pressed("LEFT_CLICK")
	
	#will be replacing click enablers on individual scripts with this
	#this will detirmine if the click was on the HUD or not, then what was under the mouse via raycast
	if LEFT_CLICK:
		pass
	if Input.is_action_just_released("skip"):
		_on_next_round_timeout()


func spawn_units(groups):
	for i in range(groups):
		var PLATOON = get_platoon.instance()
		PLATOON.position = get_node("Characters/Spawn" + str(i + 1)).position
		PLATOON.name = "platoon" + str(i)
		PLATOON.group = i + 1
		add_child(PLATOON)
		UNITS.append(PLATOON)


func select(unit):
	selected.append(unit)
	get_node("Canvas/HUD/Platoon" + str(unit.group)).modulate.a = 1


func unselect(unit):
	get_node("Canvas/HUD/Platoon" + str(unit.group)).modulate.a = .5
	selected.erase(unit)

func assign_labels():
	if $next_round.time_left <= 0:
		get_node("Canvas/HUD/next_wave").set_text("")
	else:
		get_node("Canvas/HUD/next_wave").set_text("Until Next Wave: " + str(int($next_round.time_left)))

#Called when wave is finished
func next_wave():
	get_node("Canvas/HUD/skip").text = "Enter to Skip to Next Wave"
	music.get_node("AnimationPlayer").play("change_track")
	music.play()
	active_wave = false
	$next_round.start()




#start round
func _on_next_round_timeout():
	get_node("Canvas/HUD/hint").text = ""
	get_node("Canvas/HUD/hint2").text = ""
	get_node("Canvas/HUD/hint3").text = ""
	$next_round.stop()
	music.get_node("AnimationPlayer").play("change_track")
	music.play()
	#$check_alive.start()
	active_wave = true
	print("start round")
	wave += 1
	get_node("Canvas/HUD/Wave").set_text("Wave: " + str(wave))
	get_node("Canvas/HUD/skip").text = ""
	
	if wave in [1,6,11,16,21,26,31,36,41,46,51,56,61,66,71,76,81,86,91,96]:
		spawn_enemies(10,"top", 1, 100, 200, 35, [5,10])
	
	elif wave in [2,7,12,17,22,27,32,37,42,47,52,56,62,67,72,77,82,87,92,97]:
		spawn_enemies(5,"left", 1, 100, 200, 35, [5,10])
		spawn_enemies(5,"right", 1, 100, 200, 35, [5,10])
	elif wave in [3,8,13,18,23,28,33,38,43,48,53,58,63,68,73,78,83,88,93,98]:
		spawn_enemies(10, "top", 2, 200, 400, 35, [10,15])
	elif wave in [4,9,14,19,24,29,34,39,44,49,54,59,64,69,74,79,84,89,94,99]:
		spawn_enemies(10, "top", 2, 200, 400, 35, [10,15])
		spawn_enemies(5,"left", 1, 100, 200, 35, [5,10])
		spawn_enemies(5,"right", 1, 100, 200, 35, [5,10])
	else:
		spawn_enemies(15,"left", 1, 100, 200, 35, [5,10])
		spawn_enemies(15,"right", 1, 100, 200, 35, [5,10])


#type 1 = reg, type 2 = purple, type 3 = boss
# regular = 10,"top", 2, 100, 200, 35, [5,10]
var NAME_NUM = 0
func spawn_enemies(amount, wall, type, health, attack_range, speed, damage):
	for i in range(amount):
		NAME_NUM += 1
		randomize()
		var POSITION
		var ENEMY = enemy_ref.instance()
		
		if wall == "right":
			POSITION = Vector2(rand_range(enemy_spawns.right_side[0].x,enemy_spawns.right_side[1].x), rand_range(enemy_spawns.right_side[0].y,enemy_spawns.right_side[1].y))
		if wall == "left":
			POSITION = Vector2(rand_range(enemy_spawns.left_side[0].x,enemy_spawns.left_side[1].x), rand_range(enemy_spawns.left_side[0].y,enemy_spawns.left_side[1].y))
		if wall == "top":
			POSITION = Vector2(rand_range(enemy_spawns.top_side[0].x,enemy_spawns.top_side[1].x), rand_range(enemy_spawns.top_side[0].y,enemy_spawns.top_side[1].y))
		
		ENEMY.position = POSITION
		ENEMY.name = "enemy" + str(NAME_NUM)
		ENEMY.group = "enemy"
		ENEMY.HEALTH = health
		ENEMY.RANGE = attack_range
		ENEMY.SPEED = speed
		ENEMY.TYPE = type
		ENEMY.DAMAGE = damage
		enemies_left += 1
		add_child(ENEMY)


func check_enemy_count():
	if enemies_left <= 0:
		next_wave()

func _on_check_alive_timeout():
	pass
#	for k in alive_enemies:
#		var i = 0
#		if is_instance_valid(k) == false:
#			alive_enemies.remove(i)
#		i += 1
#	if alive_enemies.size() <= 0:
#		next_wave()
#		$check_alive.stop()

var track = 1

func track_change():
	track += 1
	var playing = music.get("stream")

	match track:
		1:
			music.set("stream", off_wave1)
			music.volume_db = -5
			music.play()
		
		2:
			music.set("stream", combat1)
			music.volume_db = -15
			music.play()
		3:
			music.set("stream", off_wave2)
			music.volume_db = -5
			music.play()
		4:
			music.set("stream", combat2)
			music.volume_db = -15
			music.play()
		5:
			music.set("stream", off_wave4)
			music.volume_db = -5
			music.play()
		6:
			music.set("stream", maintheme)
			music.volume_db = -15
			music.play()
			track = 0
	print(playing)

func _on_area_body_entered(body):
	if typeof(body.group) == TYPE_STRING:
		Global.game_over()
