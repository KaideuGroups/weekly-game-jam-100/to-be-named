extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	Global.toggle_pause_game(true)
	animate_scene()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


#Will show the overlay in the detailed fashion
#if empty, scene will just pop up.
func animate_scene():
	pass


func _on_Reset_pressed():
	Global.reset_game()
