extends Node2D

export (int) var group = null
export (int) var size = 20

var get_unit = preload("res://Scenes/units/Soldier.tscn")
var defence_texture = preload("res://Assets/Units/defence_unit.png")
var support_texture = preload("res://Assets/Units/support.png")
var sniper_texture = preload("res://Assets/Units/sniper.png")

onready var range_node = get_node("Range")


var selected = false
var able = false
var offsets = [Vector2(0, 12.5), Vector2(25, 12.5), Vector2(-25, 12.5), Vector2(0, 37.5), Vector2(-25, 37.5), 
Vector2(25, 37.5), Vector2(50, 12.5), Vector2(-50, 12.5), Vector2(50, 37.5), Vector2(-50, 37.5), 
Vector2(-25, -12.5), Vector2(25, -12.5), Vector2(50, -12.5), Vector2(-50, -12.5), Vector2(-25, -37.5), 
Vector2(25, -37.5), Vector2(0, -37.5), Vector2(0, -12.5), Vector2(50, -37.5), Vector2(-50, -37.5)]

var avg_pos = Vector2(0,0)
var max_life = 0


enum MODE {FREE, HOLD}
export (MODE) var state = MODE.FREE

var platoon_target = null

var RANGE = 250
var DAMAGE = [3,7]
var SPEED = 50
var FIRE_RATE = [3,6]
#lower the number the more accurate. (Radians from centre of cone)
var ACCURACY = 0.08

var moving = false

func _ready():
	set_process(true)
	range_node.get_node("Col").set_shape(range_node.get_node("Col").get_shape().duplicate(true))
	range_node.get_node("Col").get_shape().set_radius(RANGE)
	if group == 5 or group == 6:
		size = 10
	for k in range(size):
		var s = get_unit.instance()
		s.position = get_parent().position + offsets[k]
		s.get_pos = position - offsets[k]
		add_child(s)
		avg_pos += offsets[k]
	avg_pos /= offsets.size()
	change_type(group)
	for k in get_children():
		if k.is_in_group("Soldier"):
			max_life += k.health




func _process(delta):
	if moving:
		var soldiers = get_children().size() - 2
		if soldiers >= 1:
			avg_pos =  Vector2()
			for s in get_children():
				if s.is_in_group("Soldier"):
					
					avg_pos += s.position
			avg_pos /= soldiers
			$Range.position = avg_pos
		else:
			for s in get_children():
				if s.is_in_group("Soldier"):
					$Range.set_global_position(s.get_global_positon())

		#targeting
	if is_instance_valid(platoon_target) == false or check_if_freed(platoon_target):
		relay_target(true, null)
		platoon_target = null
		if $look4target.is_stopped() == true:
			$look4target.start()
	elif check_if_freed(platoon_target) == false:
		var DISTANCE = ($Range.global_position - platoon_target.get_global_position()).length()
		if DISTANCE > RANGE + 30:
			print("out of range   " + str(DISTANCE))
			platoon_target = null
			relay_target(true, null)
			if $look4target.is_stopped() == true:
				$look4target.start()
		else:
			$look4target.stop()
			relay_target(false, platoon_target)


func _input(event):
	var LEFT_CLICK = Input.is_action_just_released("LEFT_CLICK")
	var GROUP = Input.is_action_just_pressed("Group" + str(group))
	
	if GROUP or (LEFT_CLICK and able):
		selected = !selected
		toggle_select(selected)
		if selected:
			for k in get_parent().UNITS:
				if k.selected and k.group != group:
					k.selected = false
					k.toggle_select(k.selected)


func toggle_select(select):
	if !select:
		modulate.a = 1
		Global.current_scene.unselect(self)
	else:
		modulate.a = .7
		Global.current_scene.select(self)


func relay_target(hold, trgt):
	if hold == false:
		for s in get_children():
			if s.name != "Range" and s.name != "look4target":
				s.target = trgt
	if hold == true:
		for s in get_children():
			if s.name != "Range" and s.name != "look4target":
				s.target = null



func _on_look4target_timeout():
	for k in range_node.get_overlapping_bodies():
		if k.name.substr(0,5) == "enemy":
			platoon_target = k



#func get_target():
#	for k in range_node.get_overlapping_bodies():
#		if k.name.substr(0,5) == "enemy" and check_if_freed(k) == false:
#				platoon_target = k

func _on_Range_body_entered(body):
	if platoon_target == null:
		if body.name.substr(0,5) == "enemy" and check_if_freed(body) == false:
			platoon_target = body




#change unit type based on what group it is in
func change_type(group):
	
	match group:
	#soldier
		1:
			for s in get_children():
				if s.is_in_group("Soldier"):
					s.health = 150
					FIRE_RATE = [4,8]
			RANGE = 275
			SPEED = 60

	#soldier
		2:
			for s in get_children():
				if s.is_in_group("Soldier"):
					s.health = 150
					FIRE_RATE = [4,8]
			RANGE = 275
			SPEED = 60 
	
	#defence
		3:
			for s in get_children():
				if s.is_in_group("Soldier"):
					s.get_node("Sprite").texture = defence_texture
					s.health = 300
					FIRE_RATE = [4,8]
			RANGE = 250
			SPEED = 40 

	#defence
		4:
			for s in get_children():
				if s.is_in_group("Soldier"):
					s.get_node("Sprite").texture = defence_texture
					s.health = 300
					FIRE_RATE = [4,8]
			RANGE = 250
			SPEED = 40
	#scout
		5:
			for s in get_children():
				if s.is_in_group("Soldier"):
					s.get_node("Sprite").texture = sniper_texture
					s.health = 90
					FIRE_RATE = [8,10]
			DAMAGE = [20,30]
			ACCURACY = 0.02
			RANGE = 350
			SPEED = 100
		
	#suport
		6:
			for s in get_children():
				if s.is_in_group("Soldier"):
					s.get_node("Sprite").texture = support_texture
					s.health = 150
					FIRE_RATE = [0.2,0.2]
			ACCURACY = .5
			RANGE = 500
			SPEED = 25
			
			
	get_node("Range/Col").shape.set_radius(RANGE)
	$Range/range_circle.set_scale(Vector2(RANGE / 250, RANGE / 250))
	$Range/range_circle.modulate.a = 0.08
	
	
func check_if_freed(node):
	if node != null:
		var wr = weakref(node)
		if (!wr.get_ref()):
			return  true
		else:
			return  false
	else:
		return true




